﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeLabLib
{
    public class BinaryTree<KeyType, ValueType> : IEnumerable<KeyType>
           where KeyType : IComparable<KeyType>
    {
        public BinaryTree()
        {
            Root = null;
        }

        public Node Insert(KeyType key, ValueType value)
        {
            Node input = new Node(key, value);
            if (Root == null)
                Root = input;
            else
            {
                Node currentNode = Root;
                bool isPlaced = false;
                while (!isPlaced)
                {
                    if (currentNode.Key.CompareTo(input.Key) == -1)
                    {
                        //go down right side
                        if (currentNode.RightChild == null)
                        {
                            currentNode.RightChild = input;
                            isPlaced = true;
                        }
                        else
                            currentNode = currentNode.RightChild;
                    }
                    else if (currentNode.Key.CompareTo(input.Key) == 0)
                    {
                        currentNode = input;
                        isPlaced = true;
                    }
                    else
                    {
                        //go down left side
                        if (currentNode.LeftChild == null)
                        {
                            currentNode.LeftChild = input;
                            isPlaced = true;
                        }
                        else
                            currentNode = currentNode.LeftChild;
                    }
                }
            }
            return input;
        }

        public void InsertAtRoot(KeyType key, ValueType value)
        {
            Node input = new Node(key, value);
            if (Root == null)
                Root = input;
            else
            {
                if (Root.Key.CompareTo(input.Key) == -1)
                {
                    //go down right side
                    if (Root.RightChild != null)
                        InsertAtNode(Root.RightChild, input);
                    Root.RightChild = input;
                }
                else if (Root.Key.CompareTo(input.Key) == 0)
                    Root = input;
                else
                {
                    //go down left side
                    if (Root.LeftChild != null)
                        InsertAtNode(Root.LeftChild, input);
                    Root.LeftChild = input;
                }
                Rotate(Root, input);
            }
        }

        public void InsertAtNode(Node parentNode, Node inputNode)
        {
            if (parentNode == null)
                parentNode = inputNode;
            else
            {
                if (parentNode.Key.CompareTo(inputNode.Key) == -1)
                {
                    //go down right side
                    if (parentNode.RightChild != null)
                        InsertAtNode(parentNode.RightChild, inputNode);
                    parentNode.RightChild = inputNode;
                }
                else if (parentNode.Key.CompareTo(inputNode.Key) == 0)
                    parentNode = inputNode;
                else
                {
                    //go down left side
                    if (parentNode.LeftChild != null)
                        InsertAtNode(parentNode.LeftChild, inputNode);
                    parentNode.LeftChild = inputNode;
                }
                Rotate(parentNode, inputNode);
            }
        }

        private void Rotate(Node parent, Node child)
        {
            //find which side to rotate
            if (parent.LeftChild == child)
            {
                if (child.RightChild == null)
                    parent.LeftChild = null;
                else
                    parent.LeftChild = child.RightChild;
                child.RightChild = parent;
            }
            else if (parent.RightChild == child)
            {
                if (child.LeftChild == null)
                    parent.RightChild = null;
                else
                    parent.RightChild = child.LeftChild;
                child.LeftChild = parent;
            }
            else
                throw new DataMisalignedException();
            if (Root == parent)
                Root = child;
        }

        public IEnumerable<Tuple<KeyType, ValueType>> InOrderTraversal()
        {
            if (Height() == -1)
                throw new IndexOutOfRangeException();
            // for now it can return the tuple, i need to write how it goes through the tree
            Tuple<KeyType, ValueType> returnTuple =
                      new Tuple<KeyType, ValueType>(Root.Key, Root.Value);

            if (Root.LeftChild != null)
            {
                foreach (Tuple<KeyType, ValueType> nodeValue in InOrderTraversal(Root.LeftChild))
                {
                    yield return nodeValue;
                }
            }

            yield return returnTuple;

            if (Root.RightChild != null)
            {
                foreach (Tuple<KeyType, ValueType> nodeValue in InOrderTraversal(Root.RightChild))
                {
                    yield return nodeValue;
                }
            }

        }

        public IEnumerable<Tuple<KeyType, ValueType>> InOrderTraversal(Node startingNode)
        {
            Tuple<KeyType, ValueType> returnTuple =
                new Tuple<KeyType, ValueType>(startingNode.Key, startingNode.Value);

            if (startingNode.LeftChild != null)
            {
                foreach (Tuple<KeyType, ValueType> nodeValue in InOrderTraversal(startingNode.LeftChild))
                {
                    yield return nodeValue;
                }
            }

            yield return returnTuple;

            if (startingNode.RightChild != null)
            {
                foreach (Tuple<KeyType, ValueType> nodeValue in InOrderTraversal(startingNode.RightChild))
                {
                    yield return nodeValue;
                }
            }
        }

        public Node Root { get; private set; }
        public int Height()
        {
            int height;
            if (Root == null)
                height = -1;
            else
            {
                //uses helper function that recursively counts the height
                height = Height(Root);
            }
            return height;
        }
        //recursively look down each child and take the greater num
        private int Height(Node startingNode)
        {
            //from a astarting Node, it will compare the height of the two children and take the largest
            // heigth of the left child
            int leftSideTotal = 0;
            // height of the right child
            int rightSideTotal = 0;
            int totalHeight;
            if (!hasChild(startingNode))
                totalHeight = 0;
            else
            {
                if (startingNode.LeftChild != null)
                    leftSideTotal = Height(startingNode.LeftChild) + 1;
                if (startingNode.RightChild != null)
                    rightSideTotal = Height(startingNode.RightChild) + 1;

                if (leftSideTotal > rightSideTotal)
                    totalHeight = leftSideTotal;
                else
                    totalHeight = rightSideTotal;
            }
            return totalHeight;
        }

        private bool hasChild(Node theNode)
        {
            bool dynasty = true;
            if (theNode.LeftChild == null && theNode.RightChild == null)
                dynasty = false;
            return dynasty;
        }

        //node class nested inside the BinaryTree class

        IEnumerator<KeyType> IEnumerable<KeyType>.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
        public class Node 
        {
            Node[] child = new Node[2] { null, null };
            enum Child { Left = 0, Right = 1 };
            public KeyType Key;
            public ValueType Value;

            public Node(KeyType key, ValueType value)
            {
                Key = key;
                Value = value;
            }

            public Node LeftChild
            {
                get
                {
                    return child[(int)Child.Left];
                }
                set
                {
                    child[(int)Child.Left] = value;
                }
            }

            public Node RightChild
            {
                get
                {
                    return child[(int)Child.Right];
                }
                set
                {
                    child[(int)Child.Right] = value;
                }
            }

        }
    }
}
